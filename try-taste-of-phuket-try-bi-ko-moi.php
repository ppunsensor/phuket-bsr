<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Phuket BSR - Don’t miss - Try Taste of Phuket, Try Bi-Co-Moi</title>
		<meta name="Keywords" content="Phuket BSR, Buy, Rent, Sale, Property, Boat, Don't miss" />
<meta name="Description" content="Phuket BSR - Buy-Sale-Rent property in Phuket" />		
		<?php 
			include('includes/meta.php');
		 ?>
	</head>
	<body>
		<?php 
			include('includes/topbar.php');
		 ?>
		<!-- /.topbar -->		
		<?php 
			include('includes/nav.php');
		 ?>
		<div class="page-featured-image"> 			
			<img src="images/dontmiss-header.jpg" alt="" class="img-responsive">		
		</div>
		<!-- /.search -->
		<?php 
			include('includes/search.php');
		 ?>	
		<!-- /.search -->		
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12 bg-white">
						<ul class="breadcrumb has-top-xs-space">
						    <li><a href="index.php">Home</a></li>
						    <li><a href="dont-miss.php">Don't miss</a></li>
						    <li class="active">Try Taste of Phuket, Try Bi-Co-Moi</li>
						</ul>
					</div>
				</div>	
				<div class="row bg-white">
					<div class="col-xs-12 page-title">
						<h1>Try Taste of Phuket, Try Bi-Co-Moi</h1><br>

					</div>
				</div>
				<div class="row bg-white">
					<div class="col-xs-12 text-larger has-bottom-space">
						<img src="images/featured-image-story1.jpg" class="img-responsive" alt="Story 1">
						<br>
						<p>
						<strong class="text-danger">What is Lorem Ipsum?</strong><br>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

<p>
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
<br>
<img src="images/story1-image.jpg" class="img-responsive" alt="Story 1">
<br>
<p><strong class="text-danger">Where can I get some?</strong><br>
There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>

<p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary</p>

<p>
making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. </p>


					</div>
				</div>
			</div>
			

				
		<?php 

			include('includes/footer.php');
		 ?>
	</body>
</html>