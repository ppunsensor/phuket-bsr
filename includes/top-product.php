<!-- Story 2 -->			
				<section class="story box-bg-primary has-top-space">
					<div class="row">
						<div class="col-sm-5 col-sm-offset-1 has-top-space has-bottom-space">
							<img src="images/story2.jpg" alt="Top Home" class="img-responsive">
						</div>
						<div class="col-sm-5 has-top-space has-bottom-space text-white">
							<br><br>
							<h1><strong>Top</strong> <span class="custom-text-success">Home</span></h1>
							<h2>Special offer</h2>
							<p>
								In the southwest of Phuket, located between the two famous beaches of Kata and Kata Noi, the villa offers unobstructed sea view over the Andaman Sea.
							</p>
							<a href="#property_1" data-toggle="modal" data-target="#property_1" class="btn btn-lg btn-danger" title="Top Home">Read more</a>
						</div>
					</div>
				</section>			
				<!-- /Story2 -->