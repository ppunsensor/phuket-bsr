<div class="topbar">
			<div class="container"> 
				<div class="row">
					<div class="col-xs-6 text-left">
						<i class="glyphicon glyphicon-earphone"> 095-039-7373</i>   
						<i class="glyphicon glyphicon-send"></i>  <a href="mailto:phuketBSR@yahoo.com"> phuketBSR@yahoo.com</a>
					</div>				
					<div class="col-xs-6 text-right">
						<div class="social">
							<span class="facebook">
							<a href="https://facebook.com/PhuketBSR" target="_blank" title="Follow us on Facebook">
								<img src="images/facebook-icon.png" alt="Facebook">
							</a>
							</span>
							<span class="line" style="position:relative;">
						<a href="#" data-toggle="collapse" data-target="#line"><img src="images/line-icon.png" alt="Line"></a>
						<div id="line" class="collapse">
	                      <div class="bubble-top">
	                        <img src="images/line-qrcode.png" alt="LINE">
	                      </div>
	                    </div>
						</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Logo -->
		<div class="bg-primary">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="index.php"><img src="images/logo.png" class="img-responsive center-block" alt="Phuket BSR"></a>
					</div>
				</div class="row">
			</div class="container">
		</div>