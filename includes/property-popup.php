
<!-- Popup -->
<div class="modal fade" id="property_1" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Residence and Showroom </h3>
      </div>
      <div class="modal-body">
      <!-- Gallery --> 
    <!-- main slider carousel -->
    <div class="row">
        <div class="col-md-12" id="slider">
            
                <div class="col-md-12" id="carousel-bounding-box">
                    <div id="popupPropertyCarousel" class="carousel slide">
                        <!-- main slider carousel items -->
                        <div class="carousel-inner">
                            <div class="active item" data-slide-number="0">
                                <img src="images/property/property1.jpg" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="1">
                              <img src="http://placehold.it/800x533&amp;text=two" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="2">
                                <img src="http://placehold.it/800x533&amp;text=three" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="3">
                                <img src="http://placehold.it/800x533&amp;text=four" class="img-responsive">
                            </div>
                            <div class="item" data-slide-number="4">
                                <img src="http://placehold.it/800x533&amp;text=five" class="img-responsive">
                            </div>                                        
                        </div>
                        <!-- main slider carousel nav controls --> <a class="carousel-control left" href="#popupPropertyCarousel" data-slide="prev">‹</a>

                        <a class="carousel-control right" href="#popupPropertyCarousel" data-slide="next">›</a>
                    </div>
                </div>

        </div>
    </div>
    <!--/main slider carousel-->
    <br>
     <!-- thumb navigation carousel -->
    <div class="col-md-12 hidden-sm hidden-xs" id="slider-thumbs">
        
            <!-- thumb navigation carousel items -->
          <ul class="list-inline">
          <li> <a id="carousel-selector-0" class="selected">
            <img src="http://placehold.it/80x60&amp;text=one" class="img-responsive">
          </a></li>
          <li> <a id="carousel-selector-1">
            <img src="http://placehold.it/80x60&amp;text=two" class="img-responsive">
          </a></li>
          <li> <a id="carousel-selector-2">
            <img src="http://placehold.it/80x60&amp;text=three" class="img-responsive">
          </a></li>
          <li> <a id="carousel-selector-3">
            <img src="http://placehold.it/80x60&amp;text=four" class="img-responsive">
          </a></li>
                <li> <a id="carousel-selector-4">
            <img src="http://placehold.it/80x60&amp;text=five" class="img-responsive">
          </a></li>          
            </ul>
        
    </div>
      <!-- /Gallery -->
        <br>
        <p><strong>Incredible Business opportunity</strong> in  much sort after area of Chalong
simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 

<p>Contact: 089 729 6244 , Email: <a href="mailto:ninaphuket@hotmail.com">ninaphuket@hotmail.com</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>       
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- / Popup -->