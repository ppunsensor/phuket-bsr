<div id="jobCarousel" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <!-- .item -->
    <div class="item active text-center">
        <div class="col-sm-4">
            <!-- box -->
            <div class="outer-box box-bg-info">
                <div class="inner-box text-white">
                    <h2 class="text-upper text-left">
                    <a href="#job_1" data-toggle="modal" data-target="#job_1" class="link-indark">
                     Residence and Showroom</a>
                    </h2>
                    <hr>
                    <p><strong>Jobs description</strong>, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>  
                    <hr>
                    <p class="plain-box-footer">
                      <strong>Contact</strong><br>
                      Tel: 061 445 9599 <br>  
                      Email: flodocopkt@gmail.com 
                    </p>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box box-bg-info">
                <div class="inner-box text-white">
                    <h2 class="text-upper text-left">
                      <a href="#job_1" data-toggle="modal" data-target="#job_1" class="link-indark">Residence and Showroom</a>
                    </h2>
                    <hr>
                    <p><strong>Jobs description</strong>, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>   
                    <hr>
                    <p class="plain-box-footer">
                      <strong>Contact</strong><br>
                      Tel: 061 445 9599 <br>  
                      Email: flodocopkt@gmail.com 
                    </p>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
          <!-- box -->
            <div class="outer-box box-bg-info">
                <div class="inner-box text-white">
                    <h2 class="text-upper text-left">
                      <a href="#job_1" data-toggle="modal" data-target="#job_1" class="link-indark">Residence and Showroom</a>
                    </h2>
                    <hr>
                    <p><strong>Jobs description</strong>, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>   
                    <hr>
                    <p class="plain-box-footer">
                      <strong>Contact</strong><br>
                      Tel: 061 445 9599 <br>  
                      Email: flodocopkt@gmail.com 
                    </p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.item -->
    <!-- .item -->
    <div class="item text-center">
        <div class="col-sm-4">
            <!-- box -->
            <div class="outer-box box-bg-info">
                <div class="inner-box text-white">
                    <h2 class="text-upper text-left">
                      <a href="#job_1" data-toggle="modal" data-target="#job_1" class="link-indark">Residence and Showroom</a>
                    </h2>
                    <hr>
                    <p><strong>Jobs description</strong>, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>   
                    <hr>
                    <p class="plain-box-footer">
                      <strong>Contact</strong><br>
                      Tel: 061 445 9599 <br>  
                      Email: flodocopkt@gmail.com 
                    </p>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box box-bg-info">
                <div class="inner-box text-white">
                    <h2 class="text-upper text-left">
                      <a href="#job_1" data-toggle="modal" data-target="#job_1" class="link-indark">Residence and Showroom</a>
                    </h2>
                    <hr>
                    <p><strong>Jobs description</strong>, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>   
                    <hr>
                    <p class="plain-box-footer">
                      <strong>Contact</strong><br>
                      Tel: 061 445 9599 <br>  
                      Email: flodocopkt@gmail.com 
                    </p>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box box-bg-info">
                <div class="inner-box text-white">
                    <h2 class="text-upper text-left">
                      <a href="#job_1" data-toggle="modal" data-target="#job_1" class="link-indark">Residence and Showroom</a>
                    </h2>
                    <hr>
                    <p><strong>Jobs description</strong>, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>   
                    <hr>
                    <p class="plain-box-footer">
                      <strong>Contact</strong><br>
                      Tel: 061 445 9599 <br>  
                      Email: flodocopkt@gmail.com 
                    </p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.item -->
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#jobCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-menu-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#jobCarousel" data-slide="next">
    <span class="glyphicon glyphicon-menu-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php 
  include('job-popup.php');
 ?>