<div id="eventCarousel" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <!-- .item -->
    <div class="item active text-center">
        <div class="col-sm-4">
             <!-- box -->
            <div class="outer-box box-bg-black">
                 <a href="#event_1" data-toggle="modal" data-target="#event_1"><img src="images/event1.jpg" alt="event" class="img-responsive"></a>
                 <div class="inner-box text-white">                   
                    <h2 class="text-upper text-left paddingless">
                       <a href="#event_1" data-toggle="modal" data-target="#event_1" title="Title" class="link-indark">Residence and 
Showroom</a>
                    </h2>
                    <br>                   
                    <p class="plain-box-footer">Incredible Business opportunity in 
much sort after area of Chalong
a galley of type and scrambled it to make a type specimen book.</p>                 
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
            <!-- box -->
            <div class="outer-box box-bg-black">
                 <a href="#event_1" data-toggle="modal" data-target="#event_1"><img src="images/event2.jpg" alt="event" class="img-responsive"></a>
                 <div class="inner-box text-white">                   
                    <h2 class="text-upper text-left paddingless">
                      <a href="#event_1" data-toggle="modal" data-target="#event_1" title="Title" class="link-indark">Residence and 
Showroom</a>
                    </h2>
                    <br>                   
                    <p class="plain-box-footer">Incredible Business opportunity in 
much sort after area of Chalong
a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box box-bg-black">
                 <a href="#event_1" data-toggle="modal" data-target="#event_1"><img src="images/event2.jpg" alt="event" class="img-responsive"></a>
                 <div class="inner-box text-white">                   
                    <h2 class="text-upper text-left paddingless">
                      <a href="#event_1" data-toggle="modal" data-target="#event_1" title="Title" class="link-indark">Residence and 
Showroom</a>
                    </h2>
                    <br>                   
                    <p class="plain-box-footer">Incredible Business opportunity in 
much sort after area of Chalong
a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.item -->
    <!-- .item -->
    <div class="item text-center">
        <div class="col-sm-4">
              <!-- box -->
            <div class="outer-box box-bg-black">
                 <a href="#event_1" data-toggle="modal" data-target="#event_1"><img src="images/event1.jpg" alt="event" class="img-responsive"></a>
                 <div class="inner-box text-white">                   
                    <h2 class="text-upper text-left paddingless">
                      <a href="#event_1" data-toggle="modal" data-target="#event_1" title="Title" class="link-indark">Residence and 
Showroom</a>
                    </h2>
                    <br>                   
                    <p class="plain-box-footer">Incredible Business opportunity in 
much sort after area of Chalong
a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
             <!-- box -->
            <div class="outer-box box-bg-black">
                 <a href="#event_1" data-toggle="modal" data-target="#event_1"><img src="images/event2.jpg" alt="event" class="img-responsive"></a>
                 <div class="inner-box text-white">                   
                    <h2 class="text-upper text-left paddingless">
                      <a href="#event_1" data-toggle="modal" data-target="#event_1" title="Title" class="link-indark">Residence and 
Showroom</a>
                    </h2>
                    <br>                   
                    <p class="plain-box-footer">Incredible Business opportunity in 
much sort after area of Chalong
a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
            <!-- box -->
            <div class="outer-box box-bg-black">
                 <a href="#event_1" data-toggle="modal" data-target="#event_1"><img src="images/event2.jpg" alt="event" class="img-responsive"></a>
                 <div class="inner-box text-white">                   
                    <h2 class="text-upper text-left paddingless">
                      <a href="#event_1" data-toggle="modal" data-target="#event_1" title="Title" class="link-indark">Residence and 
Showroom</a>
                    </h2>
                    <br>                   
                    <p class="plain-box-footer">Incredible Business opportunity in 
much sort after area of Chalong
a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.item -->
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#eventCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-menu-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#eventCarousel" data-slide="next">
    <span class="glyphicon glyphicon-menu-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php 
  include('event-popup.php');
 ?>