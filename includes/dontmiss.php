<section class="story bg-white has-top-xs-space">
					<div class="row">
						<div class="col-sm-6">
							<img src="images/story1.jpg" alt="Try Taste of Phuket" class="img-responsive">
						</div>
						<div class="col-sm-6">
							<br><br>
							<h1><strong>Don't</strong> <span class="custom-text-warning">Miss</span></h1>
							<h2>Try Taste of Phuket, Try Bi-Co-Moi</h2>
							<p>
								Coming to Phuket, you might have hard of several must-try local desserts such as A-Pong, O-Aew, and Bi-Co-Moi.
							</p>
							<a href="try-taste-of-phuket-try-bi-ko-moi.php" class="btn btn-lg btn-success" title="Try Taste of Phuket, Try Bi-Co-Moi">Read more</a>
						</div>
					</div>
</section>