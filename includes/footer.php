<footer class="box-bg-primary text-center">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<a href="#" title="Phuket BSR"><img src="images/logo-footer.png" alt="Phuket BSR" class="img-responsive center-block"></a>
				<p>
				79/94 Moo 4, Thepkrasattri Rd, T. Koh Keaw, A. Muang Phuket 83000, Thailand <br>Tel: 615244 Fax: 615240</p>
				<div class="social">
						<span class="facebook">
							<a href="https://facebook.com/PhuketBSR" target="_blank" title="Follow us on Facebook"><img src="images/facebook-icon.png" alt="Facebook"></a>
						</span>
						<span class="line" style="position:relative;">
							<a href="javascript:;" data-toggle="collapse" data-target="#linefooter"><img src="images/line-icon.png" alt="Line"></a>
							<div id="linefooter" class="collapse" style="position:absolute;z-index:100;top:40px;right:-58px;">
		                      <div class="bubble-top">
		                        <img src="images/line-qrcode.png" alt="LINE">
		                      </div>
		                    </div>
						</span>
				</div>

			</div>
		</div>
	</div>
</footer>



<!--  JavaScript -->
<script src="js/bsr.min.js"></script>