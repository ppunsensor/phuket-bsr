<div id="boatCarousel" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <!-- .item -->
    <div class="item active text-center">
        <div class="col-sm-4">
            <!-- box -->
            <div class="outer-box has-shadow bg-white">
                <div class="inner-box">
                    <small class="tag box-bg-warning">For Sale</small>
                    <a href="#boat_1" data-toggle="modal" data-target="#boat_1"><img src="images/boat/boat1.jpg" class="img-responsive" alt="Title"></a>
                    <h2 class="text-upper text-left">
                      Residence and Showroom
                    </h2>
                    <hr>
                    <p>Incredible Business opportunity in 
much sort after area of Chalong</p>                    
                </div>
                <div class="box-footer box-bg-success-darker">
                    <span>12,000,000 ฿</span>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box has-shadow bg-white">
                <div class="inner-box">
                  <small class="tag box-bg-warning">For Sale</small>
                    <a href="#boat_1" data-toggle="modal" data-target="#boat_1"><img src="images/boat/boat1.jpg" class="img-responsive" alt="Title"></a>
                    <h2 class="text-upper text-left">
                      Residence and Showroom
                    </h2>
                    <hr>
                    <p>Incredible Business opportunity in 
much sort after area of Chalong</p>                    
                </div>
                <div class="box-footer box-bg-success-darker">
                    <span>12,000,000 ฿</span>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box has-shadow bg-white">
                <div class="inner-box">
                  <small class="tag box-bg-warning">For Sale</small>
                    <a href="#boat_1" data-toggle="modal" data-target="#boat_1"><img src="images/boat/boat1.jpg" class="img-responsive" alt="Title"></a>
                    <h2 class="text-upper text-left">
                      Residence and Showroom
                    </h2>
                    <hr>
                    <p>Incredible Business opportunity in 
much sort after area of Chalong</p>                    
                </div>
                <div class="box-footer box-bg-success-darker">
                    <span>12,000,000 ฿</span>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.item -->
    <!-- .item -->
    <div class="item text-center">
        <div class="col-sm-4">
            <!-- box -->
            <div class="outer-box has-shadow bg-white">
                <div class="inner-box">
                  <small class="tag box-bg-warning">For Sale</small>
                    <a href="#boat_1" data-toggle="modal" data-target="#boat_1"><img src="images/boat/boat1.jpg" class="img-responsive" alt="Title"></a>
                    <h2 class="text-upper text-left">
                      Residence and Showroom
                    </h2>
                    <hr>
                    <p>Incredible Business opportunity in 
much sort after area of Chalong</p>                    
                </div>
                <div class="box-footer box-bg-success-darker">
                    <span>12,000,000 ฿</span>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box has-shadow bg-white">
                <div class="inner-box">
                  <small class="tag box-bg-warning">For Sale</small>
                    <a href="#boat_1" data-toggle="modal" data-target="#boat_1"><img src="images/boat/boat1.jpg" class="img-responsive" alt="Title"></a>
                    <h2 class="text-upper text-left">
                      Residence and Showroom
                    </h2>
                    <hr>
                    <p>Incredible Business opportunity in 
much sort after area of Chalong</p>                    
                </div>
                <div class="box-footer box-bg-success-darker">
                    <span>12,000,000 ฿</span>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <div class="col-sm-4">
           <!-- box -->
            <div class="outer-box has-shadow bg-white">
                <div class="inner-box">
                  <small class="tag box-bg-warning">For Sale</small>
                    <a href="#boat_1" data-toggle="modal" data-target="#boat_1"><img src="images/boat/boat1.jpg" class="img-responsive" alt="Title"></a>
                    <h2 class="text-upper text-left">
                      Residence and Showroom
                    </h2>
                    <hr>
                    <p>Incredible Business opportunity in 
much sort after area of Chalong</p>                    
                </div>
                <div class="box-footer box-bg-success-darker">
                    <span>12,000,000 ฿</span>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.item -->
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#boatCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-menu-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#boatCarousel" data-slide="next">
    <span class="glyphicon glyphicon-menu-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<?php 
  include('boat-popup.php');
 ?>

