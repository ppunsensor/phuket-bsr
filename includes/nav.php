<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-target" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>     
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-target">
      <ul class="nav navbar-nav navbar-center">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="top-products.php">Top Products</a></li>
        <li><a href="dont-miss.php">Don't Miss</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Properties <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="property-for-sale.php">For sale</a></li>
            <li><a href="property-for-rent.php">For rent</a></li>            
          </ul>
        </li>
        <li><a href="boats.php">Boats</a></li>
        <li><a href="wheels.php">Wheels</a></li>
        <li><a href="jobs.php">Jobs</a></li>
        <li><a href="others.php">Others</a></li>
        <li><a href="events.php">Events</a></li>       
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container -->
</nav>