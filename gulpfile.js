var gulp = require('gulp'),
	concat = require('gulp-concat'),
	cleanCss = require('gulp-clean-css'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	rename = require('gulp-rename'),
	watch = require('gulp-watch'),
	bs = require('browser-sync').create();

gulp.task('sass',function(){
 	return gulp.src(['./sources/sass/bsr.scss'])
 				.pipe(sass())				
				.pipe(gulp.dest('./sources/css'));
});

gulp.task('mincss', ['sass'], function(){
	gulp.src(['./sources/css/bsr.css', './sources/css/bubble.css'])
		.pipe(concat('bsr.min.css')) 
		.pipe(cleanCss())
		.pipe(gulp.dest('./css'))
})

gulp.task('copyfonts', function(){
	gulp.src('./node_modules/bootstrap-sass/assets/fonts/bootstrap/*.*')
		.pipe(gulp.dest('./fonts'));
});

gulp.task('minjs',function(){
	gulp.src([
			'./node_modules/jquery/dist/jquery.min.js',
			'./node_modules/bootstrap/dist/js/bootstrap.min.js',
			'./sources/js/bsr.js'
		])
		.pipe(concat('bsr.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./js'));
});

gulp.task('watch', ['browserSync'],function(){
		gulp.watch('./sources/sass/bsr.scss',['mincss']);
		gulp.watch('./css/*.css', bs.reload);
		gulp.watch('./resources/css/*.css', bs.reload);
		gulp.watch('./js/*.min.js', bs.reload);
		gulp.watch('./*.php', bs.reload);
		gulp.watch('./**/*.php', bs.reload);
		console.log('Watching...');

});

gulp.task('browserSync', function(){
	bs.init({
		proxy: 'localhost/phuket-bsr'
	});
});

gulp.task('default',['mincss', 'copyfonts', 'minjs', 'watch']);