<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Phuket BSR - Boats</title>
		<meta name="Keywords" content="Phuket BSR, Buy, Rent, Sale, Property, Boat, Wheels" />
<meta name="Description" content="Phuket BSR - Buy-Sale-Rent property in Phuket" />		
		<?php 
			include('includes/meta.php');
		 ?>
	</head>
	<body>
		<?php 
			include('includes/topbar.php');
		 ?>
		<!-- /.topbar -->		
		<?php 
			include('includes/nav.php');
		 ?>
		<div class="page-featured-image"> 			
			<img src="images/boat-header.jpg" alt="" class="img-responsive">		
		</div>
		<!-- /.search -->
		<?php 
			include('includes/search.php');
		 ?>	
		<!-- /.search -->		
			
			<div class="container">
			<div class="row">
					<div class="col-xs-12">
						<ul class="breadcrumb">
						    <li><a href="index.php">Home</a></li>
						    <li class="active">Boats</li>
						</ul>
					</div>
				</div>	
				<div class="row">
					<div class="col-xs-12 page-title">
						<h1>Boats</h1>
						<h2>Buy-Sales-Rent in Phuket</h2>
						<hr>
					</div>
				</div>
				<div class="row">
					<?php 
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
						include('includes/boat.php');
					 ?>
				</div>
				<div class="row has-top-space has-bottom-space text-center">
				<nav aria-label="Page navigation">
				  <ul class="pagination">
				    <li>
				      <a href="#" aria-label="Previous">
				        <span aria-hidden="true">&laquo;</span>
				      </a>
				    </li>
				    <li class="active"><a href="#">1</a></li>
				    <li><a href="#">2</a></li>
				    <li><a href="#">3</a></li>
				    <li><a href="#">4</a></li>
				    <li><a href="#">5</a></li>
				    <li>
				      <a href="#" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				      </a>
				    </li>
				  </ul>
				</nav>
				<?php 
					include('includes/boat-popup.php');
				 ?>	
				</div>
			</div>

				
		<?php 

			include('includes/footer.php');
		 ?>
	</body>
</html>