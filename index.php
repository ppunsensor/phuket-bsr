<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Phuket BSR</title>
		<meta name="Keywords" content="Phuket BSR, Buy, Rent, Sale, Property, Boat, Wheels" />
<meta name="Description" content="Phuket BSR - Buy-Sale-Rent property in Phuket" />		
		<?php 
			include('includes/meta.php');
		 ?>
	</head>
	<body>
		<?php 
			include('includes/topbar.php');
		 ?>
		<!-- /.topbar -->		
		<?php 
			include('includes/nav.php');
		 ?>
		<div class="featured-image"> 			
			<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<?php 
								include('includes/carousel.php');
							 ?>
						</div>
					</div>			
			</div>			
		</div>
		<!-- /.search -->
		<?php 
			include('includes/search.php');
		 ?>		
		<!-- /.search -->			
				<div class="container">
					<!-- Story 1 -->
					<section class="story bg-white">
					<div class="row">
						<div class="col-sm-6">
							<img src="images/story1.jpg" alt="Try Taste of Phuket" class="img-responsive">
						</div>
						<div class="col-sm-6">
							<br><br>
							<h1><strong>Don't</strong> <span class="custom-text-warning">Miss</span></h1>
							<h2>Try Taste of Phuket, Try Bi-Co-Moi</h2>
							<p>
								Coming to Phuket, you might have hard of several must-try local desserts such as A-Pong, O-Aew, and Bi-Co-Moi.
							</p>
							<a href="try-taste-of-phuket-try-bi-ko-moi.php" class="btn btn-lg btn-success" title="Try Taste of Phuket, Try Bi-Co-Moi">Read more</a>
						</div>
					</div>
				</section>
				<!-- /Story1 -->
				<!-- Boats -->
				<section class="has-top-space">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h3 class="text-uppercase section-title">Boats</h3>
							<h4 class="section-subtitle">Would suit a company </h4>
							<p><img src="images/section-separator.png" class="img-responsive center-block" align="center" alt=""></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<?php 
								include('includes/carousel-boat.php');
							 ?>
						</div>
					</div>
				</section>
				<!-- /Boats -->
				<!-- Wheels -->
				<section class="has-top-space">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h3 class="text-uppercase section-title">Wheels</h3>
							<h4 class="section-subtitle">Would suit a company </h4>
							<p><img src="images/section-separator.png" class="img-responsive center-block" align="center" alt=""></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<?php 
								include('includes/carousel-wheel.php');
							 ?>
						</div>
					</div>
				</section>
				<!-- /Wheels -->				

			</div>
			<!-- /.container -->

			<!-- Story 2 -->			
				<section class="story box-bg-primary has-top-space">
					<div class="container">
					<div class="row">
						<div class="col-sm-5 col-sm-offset-1 has-top-space has-bottom-space">
							<img src="images/story2.jpg" alt="Top Home" class="img-responsive">
						</div>
						<div class="col-sm-5 has-top-space has-bottom-space text-white">
							<br><br>
							<h1><strong>Top</strong> <span class="custom-text-success">Home</span></h1>
							<h2>Special offer</h2>
							<p>
								In the southwest of Phuket, located between the two famous beaches of Kata and Kata Noi, the villa offers unobstructed sea view over the Andaman Sea.
							</p>
							<a href="#property_1" data-toggle="modal" data-target="#property_1" class="btn btn-lg btn-danger" title="Top Home">Read more</a>
						</div>
					</div>
					</div>
				</section>			
				<!-- /Story2 -->

				<!-- Jobs -->
				<section class="container has-top-space">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h3 class="text-uppercase section-title">Jobs</h3>
							<h4 class="section-subtitle">Would suit a company </h4>
							<p><img src="images/section-separator.png" class="img-responsive center-block" align="center" alt=""></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<?php 
								include('includes/carousel-job.php');
							 ?>
						</div>
					</div>
				</section>
				<!-- /Jobs -->	

				<!-- Others -->
				<section class="container has-top-space">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h3 class="text-uppercase section-title">Others</h3>
							<h4 class="section-subtitle">Would suit a company </h4>
							<p><img src="images/section-separator.png" class="img-responsive center-block" align="center" alt=""></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<?php 
								include('includes/carousel-other.php');
							 ?>
						</div>
					</div>
				</section>
				<!-- /Others -->

				<!-- Event -->
				<section class="container has-top-space">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h3 class="text-uppercase section-title">Events</h3>
							<h4 class="section-subtitle">Would suit a company </h4>
							<p><img src="images/section-separator.png" class="img-responsive center-block" align="center" alt=""></p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
						<br>
							<?php 
								include('includes/carousel-event.php');
							 ?>
						</div>
					</div>
				</section>
				<!-- /Event -->	
				
				<!-- Call to action -->
				<section class="call-to-action text-center">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
							 <h1>Get 50% Discount <span class="custom-text-success">for your ads.</span></h1> 
				    <p>If you have houses, Condo, Cars or Boats for sales or rent. Maecenas convallis felis nec hendrerit ultrices. Pellentesque rutrum, libero at laoreet ornare, elit purus dictum quam, sit amet placerat metus ligula non nulla.</p> <br>
				    <a href="#" class="btn btn-lg btn-danger">Contact us</a>
						</div>
					</div>
				
				   
				</div>
				</section>
  			    <!-- / Call to action -->		
		<?php 
			include('includes/footer.php');
		 ?>
	</body>
</html>